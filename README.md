# This is simple project to demonstrate following skills

* Spring Boot with CommandLineRunner and custom logging
* fetch JSON from remote source with HttpClient
* parse/convert JSON to required format
* store resulting dataset as CSV with try-with-resources

# The task is

* grab data from some REST API (JSON) and store it locally in CSV format

# Used libraries and classes

* Spring Boot 2
* Jackson (CsvMapper/CsvSchema)
* Writer (PrintWriter/FileWriter/StringWriter)
* RestTemplate + HttpClient
* testing: JUnit5, ObjectMapper, test-specific Spring profile
