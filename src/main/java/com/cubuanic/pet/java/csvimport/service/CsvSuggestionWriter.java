package com.cubuanic.pet.java.csvimport.service;

import com.cubuanic.Generated;
import com.cubuanic.pet.java.csvimport.dto.CsvSuggestionDto;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

@Service
public class CsvSuggestionWriter {
    private CsvMapper csvMapper = new CsvMapper();
    private CsvSchema csvSchema = csvMapper
        .schemaFor(CsvSuggestionDto.class)
        .withHeader()
        .sortedBy("positionId", "countryCode", "displayName", "type", "latitude", "longitude");

    // avoid by jacoco
    @Generated(message = "Trivial")
    public void writeToFile(@NonNull final String fileName, @NonNull final List<CsvSuggestionDto> data) {
        try (Writer writer = new PrintWriter(new FileWriter(fileName))) {
            write(writer, data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void write(@NonNull final Writer writer, @NonNull final List<CsvSuggestionDto> data) throws IOException {
        csvMapper.writer().with(csvSchema).writeValues(writer).writeAll(data);
    }
}
