package com.cubuanic.pet.java.csvimport.domain;

import com.cubuanic.Generated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

// avoid by jacoco
@Generated(message = "DTO")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Suggestion {
    private long positionId;
    private String displayName;
    private String defaultName;
    private Map<String, String> translatedNames;
    private Map<String, String> translatedRegionNames;
    private String countryCode;
    private String countryNameInUserLocale;
    private double latitude;
    private double longitude;
    private long locationPositionId;
    private boolean inEurope;
    private String type;
    private long population;
    private String timeZone;
    private String code;
    private long distanceToCityCenterInMeters;
    private boolean fuzzyMatch;
    private List<String> children;
    private List<String> properties;
    private Map<String, Integer> locationDetails;
}
