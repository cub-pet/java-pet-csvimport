package com.cubuanic.pet.java.csvimport.dto;

import com.cubuanic.Generated;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

// avoid by jacoco
@Generated(message = "DTO")
@Data
@AllArgsConstructor
public class CsvSuggestionDto {
    @JsonProperty("positionId")
    private long id;
    @JsonProperty("countryCode")
    private String country;
    @JsonProperty("displayName")
    private String name;
    private String type;
    private double latitude;
    private double longitude;
}
