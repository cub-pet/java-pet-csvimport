package com.cubuanic.pet.java.csvimport;

import com.cubuanic.Generated;
import com.cubuanic.pet.java.csvimport.service.CsvSuggestionConverter;
import com.cubuanic.pet.java.csvimport.service.CsvSuggestionWriter;
import com.cubuanic.pet.java.csvimport.service.OmioApiClient;
import com.google.common.collect.ImmutableList;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@SpringBootApplication
public class CsvImporterApplication implements CommandLineRunner {
    @Autowired
    private CsvSuggestionWriter csvSuggestionWriter;

    @Autowired
    private OmioApiClient omioApiClient;

    @Autowired
    private CsvSuggestionConverter csvSuggestionConverter;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(
            new HttpComponentsClientHttpRequestFactory(
                HttpClientBuilder.create().build()
            )
        );
    }

    // avoid by jacoco
    @Generated(message = "Spring main")
    public static void main(String[] args) {
        new SpringApplicationBuilder(CsvImporterApplication.class)
            .bannerMode(Banner.Mode.OFF)
            .run(args);
    }

    // avoid by jacoco
    @Generated(message = "Trivial")
    @Override
    public void run(String... args) {
        if (args.length != 1 ) {
            System.out.println("Run with single argument, the city to search suggestions for");
            return;
        }

        final String city = args[0].trim().toLowerCase();
        final String fileName = city + ".csv";

        csvSuggestionWriter.writeToFile(
            fileName,
            omioApiClient
                .findSuggestionsByCity(city)
                .stream()
                .map(csvSuggestionConverter::toCsvSuggestionDto)
                .collect(collectingAndThen(toList(), ImmutableList::copyOf))
        );
    }
}
