package com.cubuanic.pet.java.csvimport.service;

import com.cubuanic.pet.java.csvimport.domain.Suggestion;
import com.cubuanic.pet.java.csvimport.dto.CsvSuggestionDto;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class CsvSuggestionConverter {
    public CsvSuggestionDto toCsvSuggestionDto(@NonNull final Suggestion suggestion) {
        return new CsvSuggestionDto(
            suggestion.getPositionId(),
            suggestion.getCountryCode(),
            suggestion.getDefaultName(),
            suggestion.getType(),
            suggestion.getLatitude(),
            suggestion.getLongitude()
        );
    }
}
