package com.cubuanic.pet.java.csvimport.service;

import com.cubuanic.pet.java.csvimport.domain.Suggestion;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@SpringBootTest
@ActiveProfiles("test")
class OmioApiClientTest {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private OmioApiClient omioApiClient;

    @Value("${application.suggestionUrlTemplate}")
    private String suggestionUrl;

    @Test
    void testFindSuggestionsByCity() throws JsonProcessingException {
        final Suggestion[] suggestions = {
            new Suggestion(
                376217,
                "SinCity",
                "SinCity",
                ImmutableMap.of(),
                ImmutableMap.of(),
                "DE",
                "Atlantis",
                352.52437,
                413.41053,
                376217,
                true,
                "location",
                3426354,
                "UTC",
                null,
                0,
                false,
                ImmutableList.of(),
                ImmutableList.of(),
                ImmutableMap.of()
            )
        };

        final String city = "sincity";

        MockRestServiceServer
            .bindTo(restTemplate)
            .build()
            .expect(requestTo(
                new DefaultUriBuilderFactory(suggestionUrl)
                    .builder()
                    .build(ImmutableMap.of("city", city))
            ))
            .andExpect(method(HttpMethod.GET))
            .andRespond(
                withSuccess(
                    new ObjectMapper().writeValueAsString(suggestions),
                    MediaType.APPLICATION_JSON
                )
            );

        // make sure we use test-specific profile
        assertThat(suggestionUrl, containsString("localhost"));

        assertEquals(
            ImmutableList.copyOf(suggestions),
            omioApiClient.findSuggestionsByCity(city)
        );
    }

    @Test
    void throwsOnNull() {
        assertThrows(
            NullPointerException.class,
            () -> omioApiClient.findSuggestionsByCity(null)
        );
    }
}