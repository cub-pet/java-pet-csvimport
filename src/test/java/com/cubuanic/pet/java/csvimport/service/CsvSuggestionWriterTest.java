package com.cubuanic.pet.java.csvimport.service;

import com.cubuanic.pet.java.csvimport.dto.CsvSuggestionDto;
import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CsvSuggestionWriterTest {
    private CsvSuggestionWriter csw = new CsvSuggestionWriter();
    private Writer writer = new StringWriter();
    private List<CsvSuggestionDto> data = ImmutableList.of(
        new CsvSuggestionDto(376217, "DE", "Berlin", "location", 52.52437, 13.41053),
        new CsvSuggestionDto(314826, "DE", "Berlin Tegel Airport", "airport", 52.5548, 13.28903),
        new CsvSuggestionDto(334196, "DE", "Berlin Hbf", "station", 52.525589, 13.369548)
    );

    @Test
    void testWrite() throws IOException {
        csw.write(new PrintWriter(writer), data);

        assertEquals("positionId,countryCode,displayName,type,latitude,longitude\n" +
                "376217,DE,Berlin,location,52.52437,13.41053\n" +
                "314826,DE,\"Berlin Tegel Airport\",airport,52.5548,13.28903\n" +
                "334196,DE,\"Berlin Hbf\",station,52.525589,13.369548\n",
            writer.toString()
        );
    }

    @Test
    void throwsOnNullWriter() {
        assertThrows(
            NullPointerException.class,
            () -> new CsvSuggestionWriter().write(null, data)
        );
    }

    @Test
    void throwsOnNullData() {
        assertThrows(
            NullPointerException.class,
            () -> new CsvSuggestionWriter().write(new PrintWriter(writer), null)
        );
    }
}