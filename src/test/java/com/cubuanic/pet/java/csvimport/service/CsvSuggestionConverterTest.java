package com.cubuanic.pet.java.csvimport.service;

import com.cubuanic.pet.java.csvimport.domain.Suggestion;
import com.cubuanic.pet.java.csvimport.dto.CsvSuggestionDto;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class CsvSuggestionConverterTest {
    @Test
    void toCsvSuggestionDto() {
        CsvSuggestionDto expected = new CsvSuggestionDto(
            376217,
            "DE",
            "Berlin",
            "location",
            52.52437,
            13.41053
        );

        Suggestion suggestion = new Suggestion(
            376217,
            "Berlin",
            "Berlin",
            ImmutableMap.of(),
            ImmutableMap.of(),
            "DE",
            "Germany",
            52.52437,
            13.41053,
            376217,
            true,
            "location",
            3426354,
            "Europe/Berlin",
            null,
            0,
            false,
            ImmutableList.of(),
            ImmutableList.of(),
            ImmutableMap.of()
        );

        assertEquals(
            expected,
            new CsvSuggestionConverter().toCsvSuggestionDto(suggestion)
        );
    }

    @Test
    void throwsOnNull() {
        assertThrows(
            NullPointerException.class,
            () -> new CsvSuggestionConverter().toCsvSuggestionDto(null)
        );
    }
}